"""up_platform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from up_platform import settings

urlpatterns = [
    url(r'^{}/admin/'.format(settings.URL_PRE), admin.site.urls),
    url(r'^openapi/levam/platform/', include('syn_file.urls')),  # 注意，要把app的url加到根url下。
    url(r'^{}/'.format(settings.URL_PRE), include('comk_django_account.urls')),
    url(r'^{}/'.format(settings.URL_PRE), include('comk_django_plugin.urls')),

]
