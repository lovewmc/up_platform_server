from django.db import models

from syn_file.models import Account


class FileMsg(models.Model):
    """
    接收文件的信息
    """
    WJLX_CHOICES = (
        (0, '视频'),
        (1, '音频'),
        (2, '图片'),
        (3, '文本'),
        (4, '其他'),
    )

    BZLX_CHOICES = (
        (0, '普通文件'),
        (1, '设备重点标记文件'),
    )

    wjbh = models.CharField(max_length=100, verbose_name='文件编号', primary_key=True)
    wjbm = models.CharField(max_length=100, verbose_name='文件别名', null=True, blank=True)
    pssj = models.CharField(max_length=50, verbose_name='文件别名', null=True, blank=True)
    wjdx = models.BigIntegerField(verbose_name='文件大小', null=True, blank=True)
    wjlx = models.IntegerField(verbose_name='文件类型', null=True, blank=True, choices=WJLX_CHOICES)
    wjsc = models.BigIntegerField(verbose_name='文件时长', null=True, blank=True)
    bzlx = models.IntegerField(verbose_name='文件类型备注', null=True, blank=True, choices=BZLX_CHOICES)
    bmbh = models.CharField(max_length=50, verbose_name='部门编号', null=True, blank=True)
    bmmc = models.CharField(max_length=50, verbose_name='部门名称', null=True, blank=True)
    jybh = models.CharField(max_length=50, verbose_name='用户编号', null=True, blank=True)
    jymc = models.CharField(max_length=50, verbose_name='用户名称', null=True, blank=True)
    cpxh = models.CharField(max_length=50, verbose_name='设备编号', null=True, blank=True)
    gzzxh = models.CharField(max_length=50, verbose_name='工作站编号', null=True, blank=True)
    scsj = models.CharField(max_length=50, verbose_name='生成时间', null=True, blank=True)
    ccwz = models.CharField(max_length=256, verbose_name='工作站上存储相对路径', null=True, blank=True)
    wlwz = models.CharField(max_length=256, verbose_name='工作站上存储决定路径', null=True, blank=True)
    bfwz = models.CharField(max_length=256, verbose_name='文件HTTP协议访问地址', null=True, blank=True)
    ccfwq = models.CharField(max_length=50, verbose_name='存储服务器', null=True, blank=True)
    ccxdwz = models.CharField(max_length=50, verbose_name='存储服务器上相对路径', null=True, blank=True)
    ccjdwz = models.CharField(max_length=50, verbose_name='存储服务器上绝对路径', null=True, blank=True)
    sltxdwz = models.CharField(max_length=50, verbose_name='缩略图HTTP协议访问地址', null=True, blank=True)
    sfsc = models.BooleanField(verbose_name='是否删除', default=False)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = "同步文件信息"
        verbose_name_plural = verbose_name
        db_table = 'file_msg'

    def __str__(self):
        return self.wjbh
