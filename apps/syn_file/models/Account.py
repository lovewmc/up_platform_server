from django.db import models


class Account(models.Model):
    domain = models.CharField(max_length=100, verbose_name='下联网关id', primary_key=True)
    authkey = models.CharField(max_length=100, verbose_name='请求认证', null=True, blank=True)

    class Meta:
        verbose_name = "账户"
        verbose_name_plural = verbose_name
        db_table = 'account'

    def __str__(self):
        return self.domain