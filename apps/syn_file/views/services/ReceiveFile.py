import json

from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from syn_file.views.dao.AccountDao import get_authkey
from syn_file.views.dao.FileMsgDao import save_file_msg


@method_decorator(csrf_exempt, name='dispatch')
class ReceiveFile(View):
    def post(self, request):
        try:

            request_data = request.GET.dict()

            if not isinstance(request_data, dict):
                return JsonResponse(data={'code': 1002, 'message': '请求参数有误'}, json_dumps_params={'ensure_ascii': False})
            request_domain = request_data.get('domain')
            if not request_domain:
                return JsonResponse(data={'code': 1002, 'message': 'domain为空'},
                                    json_dumps_params={'ensure_ascii': False})
            request_authkey = request_data.get('authkey')
            if get_authkey(request_domain) != request_authkey:
                return JsonResponse(data={'code': 1001, 'message': 'AuthKey验证失败'},
                                    json_dumps_params={'ensure_ascii': False})

            res = json.loads(self.request.body)
            file_datas = res.get('files')
            for file_data in file_datas:
                save_file_msg(file_data,request_domain)

            return JsonResponse(data={'code': 0, 'message': 'SUCCESS'}, json_dumps_params={'ensure_ascii': False})

        except Exception as e:
            print(e)
            return JsonResponse(data={'code': 1000, 'message': '服务器内部错误'}, json_dumps_params={'ensure_ascii': False})
