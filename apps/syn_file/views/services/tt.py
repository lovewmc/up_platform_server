import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "up_platform.settings")
django.setup()

import requests

from syn_file.models import FileMsg

if __name__ == '__main__':
    url = 'http://192.168.1.117:8000/openapi/levam/platform/upload/file/basic/info?domain=CMT00000&authkey=44030300902001199701'

    data = {
        "files": [
            {
                "bfwz": "http://192.168.56.97:80/disk0/2019-06-26/666666_44010401901491042101/666666@2019-06-26_16-08-02.mp4",
                "bmbh": "CMT000000000",
                "bmmc": "测试一部",
                "bzlx": 0,
                "ccfwq": "",
                "ccjdwz": "",
                "ccwz": "disk0/2019-06-26/666666_44010401901491042101/666666@2019-06-26_16-08-02.mp4",
                "ccxdwz": "",
                "cpxh": "44010401901491042101",
                "gzzxh": "44010401901281615429",
                "jybh": "CMT000001",
                "jymc": "小六",
                "pssj": 1565858366000,
                "scsj": 1565858366000,
                "sfsc": False,
                "sltxdwz": "http://192.168.56.97:80/disk0/2019-06-26/666666_44010401901491042101/666666@2019-06-26_16-08-02.jpg",
                "wjbh": "44010401901491042101201906261608020000201906260101558103",
                "wjbm": "666666@2019-06-26_16-08-02.mp4",
                "wjdx": 400266173,
                "wjlx": 0,
                "wjsc": 900,
                "wlwz": "C:/GOSUNCN/WSFileStoragedisk0/2019-06-26/666666_44010401901491042101/666666@2019-06-26_16-08-02.mp4"
            }
        ]
    }

    re = requests.post(url=url,json=data)
    print(re.json())
    # obj = FileMsg()
    # setattr(obj, 'wjbh', 'ads')