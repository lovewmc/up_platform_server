from syn_file.models import Account


def get_authkey(domain):
    obj = Account.objects.filter(domain=domain)
    if obj:
        return obj[0].authkey