from syn_file.models import FileMsg


def save_file_msg(data,request_domain):
    objs = FileMsg.objects.filter(wjbh=data.get('wjbh'))
    if objs:
        return objs[0]
    else:
        obj = FileMsg()
        cc = obj._meta.fields
        for c in cc:
            field_name = c.name
            if data.get(field_name):
                setattr(obj, field_name, data.get(field_name))
        obj.account_id = request_domain
        obj.save()
        return obj