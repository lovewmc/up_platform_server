from django.apps import AppConfig


class SynFileConfig(AppConfig):
    name = 'syn_file'
